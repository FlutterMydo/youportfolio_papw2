<?php

namespace YourPortfolio\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
 /**
     * Responds to requests to GET /users
     */
    public function getIndex()
    {
        //
    }

    /**
     * Responds to requests to GET /users/show/1
     */
    public function getShow($id)
    {
        //
    }

    /**
     * Responds to requests to GET /users/admin-profile
     */
    public function getVideos()
    {
        //
        Route::get('/Videos/{TipoVideo}', function($NombreVideos){
            switch ($TipoVideo) {
                case 'Academico':
                    return "Video academico";
                    break;
                case 'Laboral':
                    return "Video laboral";
                default:
                    return "Especifica un tipo de video"
                    break;
            }
        })
    }

    /**
     * Responds to requests to POST /users/profile
     */
    public function postProfile()
    {
        //
        Route::get('/posts/{id}', function($id=1){
        	return "Vista perfil {$id}";
        });
    }
}
